import React from 'react';

import Banner from '../../components/Home/Banner/Banner';
import Presentation from '../../components/Home/Presentation/Presentation';
import Testimonials from '../../components/Home/Testimonials/Testimonials';

class Home extends React.Component {
  render() {
    return (
      <div>
        <Banner></Banner>
        <Presentation></Presentation>
        <Testimonials></Testimonials>
      </div>
    );
  }
}

export default Home;
