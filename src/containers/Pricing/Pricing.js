import React from 'react';
import styles from './Pricing.module.scss';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';
import Heading from '../../components/UI/Heading/Heading';

class Pricing extends React.Component {
  render() {
    let rozmiary = [
      122,
      128,
      134,
      140,
      146,
      152,
      158,
      164,
      170,
      176,
      180,
      185,
      190,
      195,
    ];

    let rozmiarowka = null;

    let table = <Spinner></Spinner>;

    if (this.props.prices) {
      rozmiarowka = (
        <ul>
          <p>Wzrost/Rozmiar</p>
          {rozmiary.map((elm) => {
            return <li key={elm}>{elm} cm</li>;
          })}
        </ul>
      );

      table = this.props.prices.map((item, product) => {
        return (
          <ul key={product}>
            <p>{item.title}</p>
            {item.prices.map((price, size) => {
              return <li key={size}>{price} zł</li>;
            })}
          </ul>
        );
      });
    }

    return (
      <section className={styles.Container}>
        <Heading>Cennik</Heading>
        <p className={styles.Helper}>&larr; Przewiń &rarr;</p>
        <div className={styles.Pricing}>
          <ul className={styles.Table}>
            {rozmiarowka}
            {table}
          </ul>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    prices: state.fetch.prices,
  };
};

export default connect(mapStateToProps, null)(Pricing);
