import down from './down.js';
import right from './right.js';
import phone from './phone.js';
import mail from './mail.js';

const icons = { down: down, right, phone: phone, mail };

export default icons;
