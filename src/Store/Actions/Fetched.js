import * as ActionTypes from './ActionTypes';
import axios from '../../Axios/Axios';

export const fetchPrices = () => {
  return (dispatch) => {
    axios.get('prices.json').then((res) => {
      dispatch({ type: ActionTypes.FETCH_PRICES, data: res.data });
    });
  };
};

export const updatePrices = (product, size, newValue) => {
  return {
    type: ActionTypes.UPDATE_PRICES,
    product: product,
    size: size,
    newValue: newValue,
  };
};
