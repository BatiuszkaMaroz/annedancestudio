import React, { Suspense } from 'react';

import Layout from './containers/Layout/Layout';
import Home from './containers/Home/Home';

import { connect } from 'react-redux';
import * as actions from './Store/Actions/Index';
import { Redirect, Route, Switch } from 'react-router-dom';
import Spinner from './components/UI/Spinner/Spinner';

const Pricing = React.lazy(() => import('./containers/Pricing/Pricing'));
const Auth = React.lazy(() => import('./containers/Auth/Auth'));

class App extends React.Component {
  componentDidMount() {
    this.props.fetchPrices();
    this.props.checkAuth();
  }

  render() {
    return (
      <>
        <Layout>
          <Switch>
            <Route path='/admin'>
              <Suspense fallback={<Spinner></Spinner>}>
                <Auth></Auth>
              </Suspense>
            </Route>
            <Route path='/cennik'>
              <Suspense fallback={<Spinner></Spinner>}>
                <Pricing></Pricing>
              </Suspense>
            </Route>
            <Route path='/start' exact>
              <Home></Home>
            </Route>
            <Route path='/oferta'></Route>
            <Redirect from='/' to='/start' exact></Redirect>
          </Switch>
        </Layout>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPrices: () => dispatch(actions.fetchPrices()),
    checkAuth: () => dispatch(actions.checkAuth()),
  };
};

export default connect(null, mapDispatchToProps)(App);
