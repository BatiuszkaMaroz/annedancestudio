import React from 'react';
import styles from './Banner.module.scss';

const Banner = (props) => {
  return (
    <section className={styles.Banner}>
      <h1 className={styles.Title}>Witamy na Anne Dance Studio :)</h1>
      <h2 className={styles.Subtitle}>
        Jesteśmy firmą krawiecką zajmującą się szyciem strojów do tańców
        towarzyskich.
      </h2>
      <p className={styles.Description}>
        Szyjemy stroje zarówno dla dzieci oraz młodzieży jaki i osób dorosłych.
        W naszej specjalizacji posiadamy długoletnie doświadczenie, z kolei w
        produkcji korzystamy ze sprawdzonych, wysokiej klasy tkanin,
        gwarantujących estetykę, funkcjonalność i komfort użytkowania.
      </p>
      <h3 className={styles.Greeting}>
        Serdecznie zapraszamy Państwa do przejrzenia naszego asortymentu oraz
        życzymy miłego dnia.
      </h3>
    </section>
  );
};

export default Banner;
