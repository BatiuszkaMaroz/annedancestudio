import React from 'react';
import styles from './Presentation.module.scss';

import sliderImages from '../../../assets/images/slider/images';
import Slider from '../../UI/Slider/Slider';
import Heading from '../../UI/Heading/Heading';

class Presentation extends React.Component {
  constructor(props) {
    super(props);
    this.articleRef = React.createRef();
    this.textRef = React.createRef();
  }

  componentDidMount() {
    const mediaQuery = window.matchMedia('(max-width: 40em)');

    if (!mediaQuery.matches) {
      document.addEventListener('scroll', this.appearText);
    } else {
      this.textRef.current.classList.add(styles.Small);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.appearText);
  }

  appearText = () => {
    const distance = this.articleRef.current.offsetTop - window.scrollY;
    if (distance < 500) {
      document.removeEventListener('scroll', this.appearText);
      this.textRef.current.style.animationName = 'slideLeft';
    }
  };

  render() {
    return (
      <section className={styles.Presentation} ref={this.articleRef}>
        <Slider class={styles.Slider} images={sliderImages}></Slider>
        <article className={styles.Article} ref={this.textRef}>
          <Heading>W naszej ofercie znajdują się:</Heading>
          <ul>
            <li>
              - koszule body do standardu, turniejowe, treningowe i frakowe,
            </li>
            <li>- koszule body do latin,</li>
            <li>- spodnie do tańców towarzyskich, standard, latin,</li>
            <li>- kamizelki do standardu,</li>
            <li>- sukienki dla początkujących tancerek,</li>
            <li>- sukienki do standardu i latin.</li>
          </ul>
          <p className={styles.Breakpoint}>
            <span className={styles.ImportantInfo}>
              *Każdy z naszych produktów pozostaje zgodny z obowiązującym
              regulaminem w zakresie wszystkich grup wiekowych.
            </span>
          </p>
          <Heading>Co nas cechuje?</Heading>
          <ul>
            <li>
              - Wszystkie stroje są szyte na miarę, każdy z dochowaniem
              niezwykłej staranności.
            </li>
            <li>- Korzystamy tylko ze sprawdzonych, wysokiej klasy tkanin.</li>
            <li>
              - Realizujemy zamówienia zarówno indywidualne jak i grupowe.
            </li>
            <li>
              - Naszym klientom gwarantujemy konkurencyjne ceny, krótkie terminy
              realizacji i dogodne formy płatności.
            </li>
            <li>
              - Zawsze z chęcią odpowiemy na państwa pytania, telefonicznie bądź
              też mailowo.
            </li>
          </ul>
        </article>
      </section>
    );
  }
}

export default Presentation;
