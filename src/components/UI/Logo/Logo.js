import React from 'react';
import styles from './Logo.module.scss';

import LogoImage from '../../../assets/images/logo.png';

const Logo = props => (
  <img className={styles.Logo} src={LogoImage} alt='Dance Logo'></img>
);

export default Logo;
