import React from 'react';
import styles from './Slider.module.scss';

class Slider extends React.Component {
  constructor(props) {
    super(props);

    this.firstSlide = React.createRef();
    this.secondSlide = React.createRef();
    this.thirdSlide = React.createRef();
  }

  state = {
    images: this.props.images,
    maxImage: this.props.images.length - 1,
    timer: null,
    currentImage: 0,
  };

  componentWillUnmount() {
    window.clearInterval(this.timer);
  }

  componentDidMount() {
    if (this.props.controlled) {
      return;
    }

    const slides = [
      this.firstSlide.current,
      this.secondSlide.current,
      this.thirdSlide.current,
    ];

    slides[0].style.left = '-100%';
    slides[1].style.left = '0%';
    slides[2].style.left = '100%';

    const images = this.state.images.map((image) => {
      const imageTag = document.createElement('img');
      imageTag.src = image;
      imageTag.ale = 'Slider Image';
      return imageTag;
    });

    this.stepForward(slides, images, this.state.currentImage);
    this.stepForwardManual = this.stepForward.bind(this, slides, images);
    this.stepBackwardManual = this.stepBackward.bind(this, slides, images);

    this.timer = setInterval(() => {
      this.stepForward(slides, images, this.state.currentImage);
    }, 2000);
  }

  stepForward(slides, images, currentImage, manual) {
    let next = currentImage >= this.state.maxImage ? 0 : currentImage + 1;

    if (manual) {
      next = currentImage;
    }

    this.setState({ currentImage: next });

    slides.forEach((slide) => {
      switch (slide.style.left) {
        case '100%':
          slide.innerHTML = '';
          slide.insertAdjacentElement('afterbegin', images[currentImage]);
          slide.style.left = '0%';
          slide.style.zIndex = '10';
          break;
        case '0%':
          slide.style.left = '-100%';
          slide.style.zIndex = '5';
          break;
        case '-100%':
          slide.style.left = '100%';
          slide.style.zIndex = '0';
          break;
        default:
          throw new Error('Slider error - forward');
      }
    });
  }

  stepBackward(slides, images, currentImage, manual) {
    let previous = currentImage <= 0 ? this.state.maxImage : currentImage - 1;

    if (manual) {
      previous = currentImage;
    }

    this.setState({ currentImage: previous });

    slides.forEach((slide) => {
      switch (slide.style.left) {
        case '100%':
          slide.style.left = '-100%';
          slide.style.zIndex = '0';
          break;
        case '0%':
          slide.style.left = '100%';
          slide.style.zIndex = '5';
          break;
        case '-100%':
          slide.innerHTML = '';
          slide.insertAdjacentElement('afterbegin', images[currentImage]);
          slide.style.left = '0%';
          slide.style.zIndex = '10';
          break;
        default:
          throw new Error('Slider error - backward');
      }
    });
  }

  goToImageHandler = (idx) => {
    window.clearInterval(this.timer);

    if (idx === this.state.currentImage) {
      return;
    } else if (idx > this.state.currentImage) {
      this.stepForwardManual(idx, true);
    } else {
      this.stepBackwardManual(idx, true);
    }
  };

  render() {
    let controls = null;
    if (this.props.controlled) {
      controls = this.state.images.map((elm, idx) => (
        <li key={idx} onClick={this.goToImageHandler.bind(this, idx)}></li>
      ));
    }

    return (
      <div className={this.props.class}>
        <div className={styles.Slider} style={{ ...this.props.style }}>
          <div
            style={{ ...this.props.slideStyle }}
            ref={this.firstSlide}
            className={styles.Slide}
          ></div>
          <div
            style={{ ...this.props.slideStyle }}
            ref={this.secondSlide}
            className={styles.Slide}
          ></div>
          <div
            style={{ ...this.props.slideStyle }}
            ref={this.thirdSlide}
            className={styles.Slide}
          ></div>
        </div>
        <ul className={styles.Dotted}>{controls}</ul>
      </div>
    );
  }
}

export default Slider;
